set shell=/bin/bash

set showmatch
set t_Co=256
set number
set relativenumber
set nocompatible    " be iMproved, required
set rtp+=~/.vim
syntax on
set hidden
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

colorscheme PaperColors
set encoding=utf-8
filetype off                  " required
let g:ale_open_list = 0

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'xuhdev/vim-latex-live-preview'
Plugin 'airblade/vim-gitgutter'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'neovimhaskell/haskell-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'mbbill/undotree'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'dense-analysis/ale'
 Plugin 'itchyny/lightline.vim'

set completeopt+=menuone
set completeopt+=noselect


call vundle#end()            " required
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

nmap <F8> :TagbarToggle<CR>

nmap :vertical resize 20:set winfixwidth

let g:livepreview_previewer = 'zathura'
"let g:livepreview_engine = 'pdflatex' . [' -shell-escape']
let g:livepreview_engine = 'pdflatex'


let g:livepreview_updatetime = '10'
:let hs_highlight_delimiters = 1
"let g:ycm_min_num_of_chars_for_completion = 1

let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'


filetype plugin indent on    " required

set expandtab
set shiftwidth=4
set softtabstop=4
set smartindent
set hlsearch

highlight Pmenu ctermfg=15 ctermbg=8 guifg=#0000ff guibg=#0000ff

" in your .vimrc
" you have to use double quotes!
"
" UltiSnips triggering
let g:UltiSnipsExpandTrigger = '<C-Space>'
let g:UltiSnipsJumpForwardTrigger = '<C-Space>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'
" Remappings
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
imap jk <esc>


map ss zfa}
map zz zd
map ; :
map <A> w
map ` <F3>

function Linsenum()
    Goyo
    set number
    set relativenumber
    endfunction

command Ln call Linenum()
command G Goyo
set showcmd
"highlight LineNr ctermfg=4 
highlight Comment ctermfg=8 cterm=italic
highlight Function ctermfg=4 cterm=italic

let g:ale_completion_enabled = 1
let g:tex_flavor = "latex"

" Math related abbreviations
iabbrev RR \mathbb{R}
iabbrev NN \mathbb{N}
iabbrev ZZ \mathbb{Z}
iabbrev QQ \mathbb{Q}
iabbrev FF \mathbb{F}
iabbrev iit \item
iabbrev ft \frametitle{
iabbrev pp \usepackage{
iabbrev sc \section{
iabbrev sss \subsection{
iabbrev gmtr \usepackage[a4paper, total={6in,10in}]{geometry}
iabbrev qq ->
iabbrev ww <-
iabbrev sc \section{
iabbrev ml \[<CR> <CR>\] <Esc>kddkA<CR><Tab>
":nnoremap K i<CR><Esc>
nnoremap <F5> :UndotreeToggle<cr>
let g:neosolarized_contrast = "normal"
let g:neosolarized_visibility = "high"

let g:ale_set_highlights = 1
let g:ale_set_balloons = 1
let g:ale_completion_enabled = 1
"let g:loaded_youcompleteme = 1
let g:ycm_auto_trigger = 1
let g:ycm_min_num_of_chars_for_completion = 1

let g:lsc_server_commands = {'c': 'clangd'}
nmap <F8> :TagbarToggle<CR>

set background=light
set colorcolumn=80

